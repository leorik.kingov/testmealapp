package ru.leorikwhite.testmealapp.api

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import ru.leorikwhite.testmealapp.data.Category
import ru.leorikwhite.testmealapp.data.Meal

@Dao
interface MealDaoService {

    @Query("SELECT * FROM Meal")
    suspend fun getAllMeal(): List<Meal>

    @Query("SELECT * FROM Category")
    suspend fun getAllCategories(): List<Category>

    @Query("SELECT * FROM Meal WHERE idMeal LIKE :idMeal")
    suspend fun selectMealById(idMeal:String?):List<Meal>

    @Query("SELECT * FROM Category WHERE idCategory LIKE :idCategory")
    suspend fun selectCategoryById(idCategory:Int?):List<Meal>

    @Insert
    suspend fun insertAllMeals(vararg meal: Meal)

    @Insert
    suspend fun insertAllCategories(vararg category: Category)

    @Update
    suspend fun updateMeal(meal: Meal)

    @Update
    suspend fun updateCategory(category: Category)

}