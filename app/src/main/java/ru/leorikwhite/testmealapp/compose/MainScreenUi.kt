package ru.leorikwhite.testmealapp.compose

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImagePainter
import coil.compose.SubcomposeAsyncImage
import coil.compose.SubcomposeAsyncImageContent
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import ru.leorikwhite.testmealapp.R
import ru.leorikwhite.testmealapp.data.Meal
import ru.leorikwhite.testmealapp.viewmodels.MainScreenViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(viewModel: MainScreenViewModel = hiltViewModel()) {
    val lazyListState = rememberLazyListState()

    Column {

        //TopBar
        Column(
            modifier = Modifier.background(
                color = Color(0xFFFFFFFF)
            )
        ) {
            //Город и QR
            Row(
                modifier = Modifier
                    .padding(top = 16.dp)
                    .height(60.dp)
            ) {
                //DropDownMenu с городами
                var expanded by remember { mutableStateOf(false) }
                //var selectedCity by remember { mutableStateOf(viewModel.cityList[0]) }
                var selectedItemIndex by remember { mutableIntStateOf(0) }
                Box {
                    ExposedDropdownMenuBox(
                        expanded = expanded,
                        onExpandedChange = {
                            expanded = !expanded
                        },
                        modifier = Modifier.width(130.dp)

                    ) {
                        TextField(
                            value = viewModel.cityList[selectedItemIndex],
                            onValueChange = {},
                            readOnly = true,
                            trailingIcon = {
                                if (!expanded) Icon(
                                    imageVector = Icons.Default.KeyboardArrowDown,
                                    modifier = Modifier.size(30.dp),
                                    contentDescription = null
                                ) else Icon(
                                    imageVector = Icons.Default.KeyboardArrowUp,
                                    modifier = Modifier.size(30.dp),
                                    contentDescription = null
                                )
                            },
                            colors = TextFieldDefaults.colors(
                                disabledContainerColor = Color(0xFFFFFFFF),
                                focusedContainerColor = Color(0xFFFFFFFF),
                                unfocusedContainerColor = Color(0xFFFFFFFF),
                                focusedIndicatorColor = Color(0xFFFFFFFF),
                                unfocusedIndicatorColor = Color(0xFFFFFFFF),
                                disabledIndicatorColor = Color(0xFFFFFFFF)
                            ),
                            textStyle = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight.Bold,
                                color = Color(0xFF222831)
                            ),
                            modifier = Modifier.menuAnchor()

                        )

                        ExposedDropdownMenu(
                            expanded = expanded,
                            modifier = Modifier.background(Color(0xFFFFFFFF)),
                            onDismissRequest = { expanded = false }) {
                            viewModel.cityList.forEachIndexed { index, item ->
                                DropdownMenuItem(text = {
                                    Text(
                                        item,
                                        fontSize = 16.sp,
                                        color = Color(0xFF222831)
                                    )
                                },
                                    modifier = Modifier.background(Color(0xFFFFFFFF)),
                                    onClick = {
                                        selectedItemIndex = index
                                        expanded = false

                                    })
                            }
                        }
                    }
                }

                //QR
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.End,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp)
                        .wrapContentSize(Alignment.CenterEnd)
                        .padding(end = 16.dp),

                    ) {
                    Box {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_qr_code),
                            contentDescription = null,
                            modifier = Modifier
                                .size(30.dp)
                        )
                    }
                }

            }


            //Баннеры
            var showBanners by remember {
                mutableStateOf(true)
            }

            //Следим за состоянием списка, чтобы убрать баннеры
            LaunchedEffect(key1 = lazyListState) {
                snapshotFlow { lazyListState.firstVisibleItemIndex }
                    .map{ index -> index > 0 }
                    .distinctUntilChanged()
                    .collect {
                        showBanners = !it
                    }
            }

            AnimatedContent(
                targetState = showBanners,
                transitionSpec = {
                    expandVertically(animationSpec = tween(150, 150)) togetherWith
                            shrinkVertically(animationSpec = tween(150))
                },
                label = "Banners"
            ) { animate ->
                if (animate) {
                    LazyRow(
                        modifier = Modifier
                    ) {
                        items(viewModel.bannerList) {
                            Image(
                                painterResource(id = it), contentDescription = null,
                                contentScale = ContentScale.FillBounds,
                                modifier = Modifier
                                    .height(150.dp)
                                    .width(320.dp)
                            )
                        }
                    }
                }

            }

            //Категории
            LazyRow(
                modifier = Modifier.padding(5.dp)
            ) {
                items(viewModel.categoryList) {
                    Card(
                        modifier = if (viewModel.selectedCategoryId.intValue != it.idCategory) {
                            Modifier
                                .padding(horizontal = 4.dp)
                                .shadow(
                                    20.dp, shape = RoundedCornerShape(10.dp),
                                    spotColor = Color(0xFFF0F0F0)
                                )
                        } else {
                            Modifier
                                .padding(horizontal = 4.dp)
                        },
                        shape = RoundedCornerShape(10.dp),
                        colors = CardDefaults.cardColors(
                            containerColor =
                            if (viewModel.selectedCategoryId.intValue == it.idCategory) {
                                Color(0xFFF4CDD6)
                            } else {
                                Color.White
                            }
                        ),
                        onClick = {
                            viewModel.selectedCategoryId.intValue = it.idCategory ?: 1
                            viewModel.categoryChange()
                        }

                    ) {
                        Text(
                            text = it.strCategory ?: "",
                            Modifier
                                .padding(10.dp)
                                .padding(horizontal = 10.dp),
                            color = if (viewModel.selectedCategoryId.intValue == it.idCategory) {
                                Color(0xFFFD3A69)
                            } else {
                                Color(0xFFC3C4C9)
                            },
                            fontSize = 13.sp,
                            fontWeight = if (viewModel.selectedCategoryId.intValue == it.idCategory)
                                FontWeight.Bold
                            else {
                                FontWeight.Normal
                            }
                        )
                    }
                }
            }
        }

        //Лента еды
        LazyColumn(state = lazyListState) {
            items(viewModel.mealList) {
                MealCard(meal = it)
            }
        }
    }
}


//Элемент списка еды
@Composable
private fun MealCard(meal: Meal) {
    meal.sumAllIngredient()
    Row(
        modifier = Modifier
            .background(Color(0xFFFFFFFF))
            .drawBehind {
                val strokeWidth = Stroke.DefaultMiter
                val y = size.height
                drawLine(
                    brush = SolidColor(Color(0xFFF3F5F9)),
                    strokeWidth = strokeWidth,
                    cap = StrokeCap.Square,
                    start = Offset.Zero.copy(y = y),
                    end = Offset(x = size.width, y = y)
                )
            }
            .padding(10.dp)
            .fillMaxWidth()

    ) {


        //Картинка
        Card(
            shape = RoundedCornerShape(10.dp)
        ) {
            SubcomposeAsyncImage(
                model = meal.strMealThumb,
                contentDescription = meal.strMeal,
                modifier = Modifier.size(150.dp),
                contentScale = ContentScale.FillBounds,
            ) {
                val state = painter.state
                if (state is AsyncImagePainter.State.Loading || state is AsyncImagePainter.State.Error) {
                    CircularProgressIndicator()
                } else {
                    SubcomposeAsyncImageContent()
                }
            }
        }


        //Часть с названием и ингредиентами
        Column(
            modifier = Modifier
                .height(150.dp)
                .fillMaxSize()
                .padding(horizontal = 10.dp),
        ) {
            Text(
                text = meal.strMeal ?: "",
                fontWeight = FontWeight.Bold,
                color = Color(0xFF222831),
                fontSize = 16.sp
            )
            Text(
                text = meal.listIngredients ?: "",
                color = Color(0xFFAAAAAD),
                fontSize = 14.sp,
                maxLines = 5
            )

            //Кнопка с ценой
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.Bottom
            ) {
                Row {
                    Button(
                        shape = RoundedCornerShape(10.dp),
                        contentPadding = PaddingValues(10.dp),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFFBFBFB)
                        ),
                        border = BorderStroke(2.dp, Color(0xFFFD3A69)),
                        onClick = {

                        }
                    ) {
                        Text(
                            text = "от 345 р",
                            modifier = Modifier.padding(horizontal = 5.dp),
                            color = Color(0xFFFD3A69),
                            fontSize = 13.sp
                        )
                    }
                }
            }

        }
    }
}
