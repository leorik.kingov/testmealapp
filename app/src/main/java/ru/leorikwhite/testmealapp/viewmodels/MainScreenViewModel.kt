package ru.leorikwhite.testmealapp.viewmodels

import android.content.Context
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import ru.leorikwhite.testmealapp.R
import ru.leorikwhite.testmealapp.data.Category
import ru.leorikwhite.testmealapp.data.Meal
import ru.leorikwhite.testmealapp.data.MealRepository
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel @Inject constructor(
    private val mealRepository: MealRepository,
    private val savedStateHandle: SavedStateHandle,
    @ApplicationContext application: Context
) : ViewModel() {

    //Полный список еды
    private val _mealList = mutableStateListOf<Meal>()

    //Показываемый список еды
    private var _showingMealList = mutableStateListOf<Meal>()

    //Список городов
    private val _cityList = listOf("Москва", "Самара", "Тула", "Волгоград")

    //Баннеры
    private val _bannersList = listOf(R.drawable.banner1, R.drawable.banner2)

    //Категории
    private val _categoryList = mutableStateListOf<Category>()
    val selectedCategoryId = mutableIntStateOf(1)

    val mealList: List<Meal>
        get() = _showingMealList

    val cityList: List<String>
        get() = _cityList

    val bannerList: List<Int>
        get() = _bannersList

    val categoryList: List<Category>
        get() = _categoryList


    private val sharedPref = application.getSharedPreferences("TestMealApp", Context.MODE_PRIVATE)


    init {
        selectedCategoryId.intValue =
            sharedPref.getInt("SelectedCategoryId", selectedCategoryId.intValue)

        getMeals()
        getAllCategory()
        categoryChange()

    }

    private fun getMeals() = viewModelScope.launch {
        _mealList.addAll(mealRepository.getAllMeal())
        mealRepository.insertAllMealsInDB(_mealList)
        categoryChange()
    }

    private fun getAllCategory() = viewModelScope.launch {
        _categoryList.addAll(mealRepository.getAllCategory())
        mealRepository.insertAllCategoriesInDB(_categoryList)
    }

    fun categoryChange() = viewModelScope.launch {
        _showingMealList.clear()
        _showingMealList.addAll(_mealList.filter { meal ->
            meal.strCategory == _categoryList.find { it.idCategory == selectedCategoryId.intValue }?.strCategory
        })
        sharedPref.edit().putInt("SelectedCategoryId", selectedCategoryId.intValue).apply()
    }

}