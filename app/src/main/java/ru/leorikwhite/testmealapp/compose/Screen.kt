package ru.leorikwhite.testmealapp.compose

import androidx.annotation.StringRes
import ru.leorikwhite.testmealapp.R

sealed class Screen(val route: String, @StringRes val resourceId: Int, val icon: Int) {
    object Menu : Screen("menu", R.string.Menu, R.drawable.ic_menu)
    object Profile : Screen("profile", R.string.Profile, R.drawable.ic_profile)
    object Bucket : Screen("friendslist", R.string.Bucket, R.drawable.ic_bucket)
}
