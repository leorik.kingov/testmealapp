package ru.leorikwhite.testmealapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)
val WhiteF0 = Color(0xFFF0F0F0)
val PinkF4 = Color(0xFFF4CDD6)
val PinkFD = Color(0xFFFD3A69)
val WhiteC3 = Color(0xFFC3C4C9)
val WhiteF3 = Color(0xFFF3F5F9)
val GreyAA = Color(0xFFAAAAAD)
val Black22 = Color(0xFF222831)
val WhiteFB = Color(0xFFFBFBFB)
val Grey7B = Color(0xFF7B7B7B)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)