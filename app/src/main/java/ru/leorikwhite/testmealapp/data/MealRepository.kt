package ru.leorikwhite.testmealapp.data

import android.content.Context
import android.net.ConnectivityManager
import dagger.hilt.android.qualifiers.ApplicationContext
import ru.leorikwhite.testmealapp.api.MealDaoService
import ru.leorikwhite.testmealapp.api.MealService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MealRepository @Inject constructor(
    private val mealApi:MealService,
    private val mealDaoService:MealDaoService,
    @ApplicationContext application: Context
) {

    val connectivityManager =
        application.getSystemService(ConnectivityManager::class.java) as ConnectivityManager

    suspend fun getAllMeal():List<Meal>{

        return if (connectivityManager.activeNetwork != null) {
            mealApi.getAllMean().meals
        } else {
            mealDaoService.getAllMeal()
        }

    }

    suspend fun getAllCategory():List<Category>{

        return if(connectivityManager.activeNetwork != null) {
            mealApi.getAllCategory().categories
        }
        else {
            mealDaoService.getAllCategories()
        }

    }

    suspend fun insertAllMealsInDB(meals:List<Meal>) {
        meals.forEach {
            val currentMeals = mealDaoService.selectMealById(it.idMeal)
            if(currentMeals.size == 0) mealDaoService.insertAllMeals(it)
        }
    }

    suspend fun insertAllCategoriesInDB(categories:List<Category>){
        categories.forEach {
            val currentCategories = mealDaoService.selectCategoryById(it.idCategory)
            if(currentCategories.size == 0) mealDaoService.insertAllCategories(it)
        }
    }

}