package ru.leorikwhite.testmealapp.di

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.leorikwhite.testmealapp.api.MealDaoService
import ru.leorikwhite.testmealapp.data.Category
import ru.leorikwhite.testmealapp.data.Meal
import javax.inject.Inject
import javax.inject.Singleton

@Database(entities = [Meal::class, Category::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun MealDaoService(): MealDaoService
}


@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule @Inject constructor(

) {

    @Singleton
    @Provides
    fun getDaoMealService(@ApplicationContext application: Context):MealDaoService{
        return getDataBaseInstance(application).MealDaoService()
    }

    @Singleton
    @Provides
    fun getDataBaseInstance(@ApplicationContext application: Context):AppDatabase{
        return Room.databaseBuilder(
            application,
            AppDatabase::class.java,
            "MealDB"
        ).build()
    }

}