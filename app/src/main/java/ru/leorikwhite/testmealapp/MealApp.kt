package ru.leorikwhite.testmealapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MealApp : Application() {
}