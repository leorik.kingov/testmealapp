package ru.leorikwhite.testmealapp.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


data class Meals (
    @SerializedName("meals" ) var meals : ArrayList<Meal> = arrayListOf()
)

@Entity

data class Meal(

    @PrimaryKey(autoGenerate = true) var idDB:Int? = null,
    @ColumnInfo("idMeal") @SerializedName("idMeal"                      ) var idMeal                      : String? = null,
    @ColumnInfo("strMeal") @SerializedName("strMeal"                     ) var strMeal                     : String? = null,
    @ColumnInfo("strCategory") @SerializedName("strCategory"                 ) var strCategory                 : String? = null,
    @ColumnInfo("strMealThumb") @SerializedName("strMealThumb"                ) var strMealThumb                : String? = null,
    @ColumnInfo("listIngredients") @SerializedName("listIngredients") var listIngredients: String? = null,
    @SerializedName("strIngredient1"              ) var strIngredient1              : String? = null,
    @SerializedName("strIngredient2"              ) var strIngredient2              : String? = null,
    @SerializedName("strIngredient3"              ) var strIngredient3              : String? = null,
    @SerializedName("strIngredient4"              ) var strIngredient4              : String? = null,
    @SerializedName("strIngredient5"              ) var strIngredient5              : String? = null,
    @SerializedName("strIngredient6"              ) var strIngredient6              : String? = null,
    @SerializedName("strIngredient7"              ) var strIngredient7              : String? = null,
    @SerializedName("strIngredient8"              ) var strIngredient8              : String? = null,
    @SerializedName("strIngredient9"              ) var strIngredient9              : String? = null,
    @SerializedName("strIngredient10"             ) var strIngredient10             : String? = null,
    @SerializedName("strIngredient11"             ) var strIngredient11             : String? = null,
    @SerializedName("strIngredient12"             ) var strIngredient12             : String? = null,
    @SerializedName("strIngredient13"             ) var strIngredient13             : String? = null,
    @SerializedName("strIngredient14"             ) var strIngredient14             : String? = null,
    @SerializedName("strIngredient15"             ) var strIngredient15             : String? = null,
    @SerializedName("strIngredient16"             ) var strIngredient16             : String? = null,
    @SerializedName("strIngredient17"             ) var strIngredient17             : String? = null,
    @SerializedName("strIngredient18"             ) var strIngredient18             : String? = null,
    @SerializedName("strIngredient19"             ) var strIngredient19             : String? = null,
    @SerializedName("strIngredient20"             ) var strIngredient20             : String? = null,
) {

    fun sumAllIngredient(){
        var ingredientsList:String? = ""
        for (i in 1..20) {
            val ingredient = javaClass.getDeclaredField("strIngredient$i").get(this) as String?
            if (!ingredient.isNullOrBlank()) {
                if(i!=1) ingredientsList += ", "
                ingredientsList += "$ingredient"
            }
        }
        listIngredients = ingredientsList
    }

}

data class Categories (
    @SerializedName("categories") var categories : ArrayList<Category> = arrayListOf()
)

@Entity
data class Category(
    @PrimaryKey(autoGenerate = true) var idDB:Int? = null,

    @ColumnInfo("idCategory") @SerializedName("idCategory") var idCategory : Int? = null,
    @ColumnInfo("strCategory") @SerializedName("strCategory") var strCategory : String? = null
) {

}
