package ru.leorikwhite.testmealapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material3.MaterialTheme
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import ru.leorikwhite.testmealapp.compose.BucketScreenUi
import ru.leorikwhite.testmealapp.compose.MainScreen
import ru.leorikwhite.testmealapp.compose.ProfileScreenUi
import ru.leorikwhite.testmealapp.compose.Screen
import ru.leorikwhite.testmealapp.compose.bottombar.BottomNavigationBar
import ru.leorikwhite.testmealapp.ui.theme.TestMealAppTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TestMealAppTheme {
                val navController = rememberNavController()
                Scaffold(
                    contentColor = MaterialTheme.colorScheme.onSurface,
                    bottomBar = {
                        BottomNavigationBar(navController = navController)
                    }
                ) { innerPadding ->
                    NavHost(
                        navController,
                        startDestination = Screen.Menu.route,
                        Modifier.padding(innerPadding)
                    ) {
                        composable(Screen.Menu.route) { MainScreen() }
                        composable(Screen.Profile.route) { ProfileScreenUi() }
                        composable(Screen.Bucket.route) { BucketScreenUi() }
                    }
                }
            }
        }

    }

}


