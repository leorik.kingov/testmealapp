package ru.leorikwhite.testmealapp.api

import retrofit2.http.GET
import ru.leorikwhite.testmealapp.data.Categories
import ru.leorikwhite.testmealapp.data.Category
import ru.leorikwhite.testmealapp.data.Meal
import ru.leorikwhite.testmealapp.data.Meals

interface MealService {

    @GET("search.php?s")
    suspend fun getAllMean(): Meals

    @GET("categories.php")
    suspend fun getAllCategory():Categories

}